class Node {

    String data;
    Node leftChild, rightChild;
    int height;

    Node(String data) {
        this.data = data;
        height = 1;
    }
}