import java.io.*;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) throws IOException {

        String fileName = args[0];
        String filePath = new File(fileName).getAbsolutePath();
        int totalLine = lineCounter(filePath);
        System.out.println("Your file contains " + totalLine + " name(s)!");
        System.out.println();


        // AVL TREE
        AVLTree tree = new AVLTree();

        try {
            File file = new File(filePath);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                tree.root = tree.insert(tree.root, st);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found! Please change the file path...");
        } catch (IOException ex) {
            System.out.println("Something went wrong!");
        }

//        System.out.println("******************************************");
//        System.out.println("In-order traversal of the AVL Tree:");
//        tree.inOrder(tree.root);
//        System.out.println();
//        System.out.println("******************************************\n");

        System.out.println("***************************************************");
        long startTimeforAVL = System.nanoTime();
        System.out.print("Inserting the name \"Ali\" into AVL Tree... ");
        tree.root = tree.insert(tree.root, "Ali");
        long stopTimeforAVL = System.nanoTime();
        long elapsedTimeAVL = stopTimeforAVL - startTimeforAVL;
        System.out.println(elapsedTimeAVL + " ns");

        long startTimeforAVL2 = System.nanoTime();
        System.out.print("Inserting the name \"Zeynep\" into AVL Tree... ");
        tree.root = tree.insert(tree.root, "Zeynep");
        long stopTimeforAVL2 = System.nanoTime();
        long elapsedTimeAVL2 = stopTimeforAVL2 - startTimeforAVL2;
        System.out.println(elapsedTimeAVL2 + " ns");
        System.out.println("***************************************************");
        System.out.println();


        //PRIORITY QUEUE
        Comparator<String> stringComparator = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        };

        // Create a Priority Queue with a custom comparator
        PriorityQueue<String> namePriorityQueue = new PriorityQueue<>(totalLine, stringComparator);

        try {
            File file = new File(filePath);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                // Add items to a Priority Queue (ENQUEUE)
                namePriorityQueue.add(st);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found! Please change the file path...");
        } catch (IOException ex) {
            System.out.println("Something went wrong!");
        }

        System.out.println("***************************************************");
        long startTimeforPriorityQueue = System.nanoTime();
        System.out.print("Inserting the name \"Ali\" into Priority Queue... ");
        namePriorityQueue.add("Ali");
        long stopTimeforPriorityQueue = System.nanoTime();
        long elapsedTimePriorityQueue = stopTimeforPriorityQueue - startTimeforPriorityQueue;
        System.out.println(elapsedTimePriorityQueue + " ns");

        long startTimeforPriorityQueue2 = System.nanoTime();
        System.out.print("Inserting the name \"Zeynep\" into Priority Queue... ");
        namePriorityQueue.add("Zeynep");
        long stopTimeforPriorityQueue2 = System.nanoTime();
        long elapsedTimePriorityQueue2 = stopTimeforPriorityQueue2 - startTimeforPriorityQueue2;
        System.out.println(elapsedTimePriorityQueue2 + " ns");
        System.out.println("***************************************************");
    }

    private static int lineCounter(String filename) throws IOException {
        try (InputStream is = new BufferedInputStream(new FileInputStream(filename))) {
            byte[] c = new byte[1024];
            int count = 0;
            int readChars = 0;
            boolean endsWithoutNewLine = false;
            while ((readChars = is.read(c)) != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n')
                        ++count;
                }
                endsWithoutNewLine = (c[readChars - 1] != '\n');
            }
            if (endsWithoutNewLine) {
                ++count;
            }
            return count;
        }
    }
}
