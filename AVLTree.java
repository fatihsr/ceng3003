class AVLTree {

    Node root;

    Node insert(Node node, String data) {
        if (node == null)
            return (new Node(data));

        if (data.compareTo(node.data) < 0)
            node.leftChild = insert(node.leftChild, data);
        else if (data.compareTo(node.data) > 0)
            node.rightChild = insert(node.rightChild, data);
        else
            return node;

        node.height = 1 + Math.max(height(node.leftChild), height(node.rightChild));

        int balance = getBalance(node);

        // Left Left Case
        if (balance > 1 && data.compareTo(node.leftChild.data) < 0)
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && data.compareTo(node.rightChild.data) > 0)
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && data.compareTo(node.leftChild.data) > 0) {
            node.leftChild = leftRotate(node.leftChild);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && data.compareTo(node.rightChild.data) < 0) {
            node.rightChild = rightRotate(node.rightChild);
            return leftRotate(node);
        }

        return node;
    }

    private Node leftRotate(Node x) {
        Node y = x.rightChild;
        Node T2 = y.leftChild;

        y.leftChild = x;
        x.rightChild = T2;

        x.height = Math.max(height(x.leftChild), height(x.rightChild)) + 1;
        y.height = Math.max(height(y.leftChild), height(y.rightChild)) + 1;

        return y;
    }

    private Node rightRotate(Node y) {
        Node x = y.leftChild;
        Node T2 = x.rightChild;

        x.rightChild = y;
        y.leftChild = T2;

        y.height = Math.max(height(y.leftChild), height(y.rightChild)) + 1;
        x.height = Math.max(height(x.leftChild), height(x.rightChild)) + 1;

        return x;
    }

    private int getBalance(Node node) {
        if (node == null)
            return 0;

        return height(node.leftChild) - height(node.rightChild);
    }

    private int height(Node node) {
        if (node == null)
            return 0;

        return node.height;
    }

    void inOrder(Node node) {
        if (node != null) {
            inOrder(node.leftChild);
            System.out.print(node.data + " ");
            inOrder(node.rightChild);
        }
    }
}